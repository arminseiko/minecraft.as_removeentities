package de.kekse23.as_removeentities;
import java.util.*;
import java.util.logging.Level;
import java.text.*;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.*;

public class RemoveEntitiesPlugin extends JavaPlugin
{
	@Override
	public void onEnable()
	{
		this.getCommand("removeentities").setExecutor(new CommandRemoveEntities());
		this.getCommand("re").setExecutor(new CommandRemoveEntities());
		this.getCommand("removeentitiesv").setExecutor(new CommandRemoveEntities());
		this.getCommand("rev").setExecutor(new CommandRemoveEntities());
	}
	@Override
	public void onDisable()
	{
	}
	
	public boolean throwUsage(Player player, String arg2)
	{
		player.sendMessage(ChatColor.RED+"/"+arg2+" <types> <radius or rx:ry:rz> [at location x:y:z] [world]");
		return true;
	}
	
	public class CommandRemoveEntities implements CommandExecutor
	{
		@Override
	    public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3)
	    {
			if(arg0 instanceof org.bukkit.entity.Player)
			{
				if(!arg0.hasPermission("de.kekse23.removeentities"))
				{
					arg0.sendMessage(ChatColor.RED+"You do not have the permission to execute this command");
	    			getLogger().log(Level.INFO, arg0.getName()+" tried to use RemoveEntities with insufficient permissions");
	    			return true;
				}
			}
	    	if(arg3.length > 1)
	    	{
	    		List<Entity> entities = null;
	    		org.bukkit.Location location = null;
	    		org.bukkit.World world = null;
	    		double radiusx = (Double) .0;
	    		double radiusy = (Double) .0;
	    		double radiusz = (Double) .0;
	    		int removed_entities = 0;
	    		boolean verbose = false;
	    		if(arg2.equals("rev") || arg2.equals("removeentitiesv"))
	    			verbose = true;
	    		else if(arg3.length > 4)
	    			verbose = Boolean.parseBoolean(arg3[4]);
	    		
	    		String[] eradius = arg3[1].split(":");
    			if(eradius.length == 3)
    			{
    				try
    				{
    					radiusx = Double.parseDouble(eradius[0]);
    					radiusy = Double.parseDouble(eradius[1]);
    					radiusz = Double.parseDouble(eradius[2]);
    				}
    				catch(Exception e)
    				{
    					arg0.sendMessage(ChatColor.RED+"Radius format invalid");
		    			getLogger().log(Level.INFO, "Radius format invalid");
		    			if (arg0 instanceof Player)
		    				return throwUsage((Player) arg0, arg2);
		    			else
		    				return true;
    				}
    			}
    			else if(eradius.length == 1)
    			{
    				try
    				{
    					radiusx = Double.parseDouble(eradius[0]);
        				radiusy = radiusx;
        				radiusz = radiusy;
    				}
    				catch(Exception e)
    				{
    					arg0.sendMessage(ChatColor.RED+"Radius format invalid");
		    			getLogger().log(Level.INFO, "Radius format invalid");
		    			if (arg0 instanceof Player)
		    				return throwUsage((Player) arg0, arg2);
		    			else
		    				return true;
    				}
    			}
    			else
    			{
    				arg0.sendMessage(ChatColor.RED+"Radius format invalid");
	    			getLogger().log(Level.INFO, "Radius format invalid");
	    			if (arg0 instanceof Player)
	    				return throwUsage((Player) arg0, arg2);
	    			else
	    				return true;
    			}

	    		if(arg3.length > 2)
	    		{	    			
		    		if(arg3.length > 3)
		    			world = getServer().getWorld(arg3[3]);
		    		else
		    		{
		    			if (arg0 instanceof Player)
		    				world = ((Player) arg0).getWorld();
		    			else if (arg0 instanceof org.bukkit.command.BlockCommandSender)
		    			{
		    				location = ((org.bukkit.command.BlockCommandSender) arg0).getBlock().getLocation();
		    				world = location.getWorld();
		    			}
		    			else
			    		{
			    			arg0.sendMessage(ChatColor.RED+"RemoveEntities without coordinates is only build to run as Player or CommandBlock");
			    			getLogger().log(Level.INFO, "RemoveEntities without coordinates is only build to run as Player or CommandBlock");
			    			return true;
			    		}
		    		}
		    		
		    		if(world != null)
		    		{
			    		String[] elocation = arg3[2].split(":");
		    			if(elocation.length == 3)
		    			{
		    				try
		    				{
		    					location = new org.bukkit.Location(world, Double.parseDouble(elocation[0]), Double.parseDouble(elocation[1]), Double.parseDouble(elocation[2]));
		    				}
		    				catch(Exception e)
		    				{
		    					arg0.sendMessage(ChatColor.RED+"Location format invalid");
				    			getLogger().log(Level.INFO, "Location format invalid");
				    			if (arg0 instanceof Player)
				    				return throwUsage((Player) arg0, arg2);
				    			else
				    				return true;
		    				}
		    				entities = new ArrayList<Entity>(world.getNearbyEntities(location, radiusx, radiusy, radiusz));
		    			}
		    			else
		    			{
		    				arg0.sendMessage(ChatColor.RED+"Location format invalid");
			    			getLogger().log(Level.INFO, "Location format invalid");
			    			if (arg0 instanceof Player)
			    				return throwUsage((Player) arg0, arg2);
			    			else
			    				return true;
		    			}
		    		}
		    		else
		    		{
		    			arg0.sendMessage(ChatColor.RED+"Requested world is not loaded");
		    			getLogger().log(Level.INFO, "Requested world is not loaded");
		    			if (arg0 instanceof Player)
		    				return throwUsage((Player) arg0, arg2);
		    			else
		    				return true;
		    		}
	    		}
	    		else if(arg0 instanceof Player)
	    		{
	    			location = ((Player) arg0).getLocation(); // only for later use
	    			world = ((Player) arg0).getWorld();
		    		entities = ((Player) arg0).getNearbyEntities(radiusx, radiusy, radiusz);
	    		}
	    		else if (arg0 instanceof org.bukkit.command.BlockCommandSender)
    			{
    				location = ((org.bukkit.command.BlockCommandSender) arg0).getBlock().getLocation();
    				world = location.getWorld();
    				entities = new ArrayList<Entity>(world.getNearbyEntities(location, radiusx, radiusy, radiusz));
    			}
	    		else
	    		{
	    			arg0.sendMessage(ChatColor.RED+"RemoveEntities without coordinates is only build to run as Player or CommandBlock");
	    			getLogger().log(Level.INFO, "RemoveEntities without coordinates is only build to run as Player or CommandBlock");
	    			return true;
	    		}
		    	
		    	String[] types = arg3[0].split(":");
		    	if(types.length == 2)
		    	{
			    	String etypes = types[1];
		    		if(etypes.length() > 0)
		    		{
		    			String[] type = etypes.split(",");
		    			if(type.length > 0)
		    			{
			    			for(String etype : type)
			    			{
			    				if(etype.length() > 0)
			    				{
			    					for(Entity entity : entities)
			    			    	{
			    						if(entity.isValid())
			    			    		{
			    							String[] subtype = etype.split("\\.");
				    						try
						    				{
				    							if(subtype.length > 1)
				    							{
				    								if(org.bukkit.entity.EntityType.DROPPED_ITEM == org.bukkit.entity.EntityType.valueOf(subtype[0].toUpperCase()))
				    								{
				    									if(entity instanceof org.bukkit.entity.Item)
				    									{
				    										if(((org.bukkit.entity.Item) entity).getItemStack().getType().name().equals(subtype[1].toUpperCase()))
				    										{
				    											if(verbose)
									    						{
									    							arg0.sendMessage(ChatColor.GRAY+"Removed specific "+entity.getType().name()+" ["+((org.bukkit.entity.Item) entity).getItemStack().getType().name()+"] at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
											    		    		getLogger().log(Level.INFO, "Removed specific "+entity.getType().name()+" ["+((org.bukkit.entity.Item) entity).getItemStack().getType().name()+"] at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
									    						}
								    							entity.remove();
								    							removed_entities++;
				    										}
				    									}
				    								}
				    								else if(subtype[1].toUpperCase().equals("TAMED"))
				    								{
				    									if(entity instanceof org.bukkit.entity.Tameable)
				    									{
				    										if(((org.bukkit.entity.Tameable)entity).isTamed())
				    										{
				    											if(verbose)
									    						{
									    							arg0.sendMessage(ChatColor.GRAY+"Removed specific tamed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
											    		    		getLogger().log(Level.INFO, "Removed specific tamed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
									    						}
								    							entity.remove();
								    							removed_entities++;
				    										}
				    									}
				    									else
				    									{
				    										arg0.sendMessage(ChatColor.RED+subtype[0].toUpperCase()+" is no tameable instance");
									    		    		getLogger().log(Level.INFO, subtype[0].toUpperCase()+" is no tameable instance");
									    		    		if (arg0 instanceof Player) 
									    		    			return throwUsage((Player) arg0, arg2);
									    		    		else 
									    		    			return true;
				    									}
				    								}
				    								else if(subtype[1].toUpperCase().equals("!TAMED"))
				    								{
				    									if(entity instanceof org.bukkit.entity.Tameable)
				    									{
				    										if(!((org.bukkit.entity.Tameable)entity).isTamed())
				    										{
				    											if(verbose)
									    						{
									    							arg0.sendMessage(ChatColor.GRAY+"Removed specific non tamed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
											    		    		getLogger().log(Level.INFO, "Removed specific non tamed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
									    						}
								    							entity.remove();
								    							removed_entities++;
				    										}
				    									}
				    									else
				    									{
				    										arg0.sendMessage(ChatColor.RED+subtype[0].toUpperCase()+" is no tameable instance");
									    		    		getLogger().log(Level.INFO, subtype[0].toUpperCase()+" is no tameable instance");
									    		    		if (arg0 instanceof Player) 
									    		    			return throwUsage((Player) arg0, arg2);
									    		    		else 
									    		    			return true;
				    									}
				    								}
				    								else
				    								{
				    									arg0.sendMessage(ChatColor.RED+"SubTypes not implemented for"+subtype[0].toUpperCase());
								    		    		getLogger().log(Level.INFO, "SubTypes not implemented for"+subtype[0].toUpperCase());
								    		    		if (arg0 instanceof Player) 
								    		    			return throwUsage((Player) arg0, arg2);
								    		    		else 
								    		    			return true;
				    								}
				    							}
				    							else if(entity.getType() == org.bukkit.entity.EntityType.valueOf(subtype[0].toUpperCase()))
						    					{
						    						if(verbose)
						    						{
						    							arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
								    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
						    						}
					    							entity.remove();
					    							removed_entities++;
						    					}
						    				}
						    				catch(Exception e)
						    				{
						    					arg0.sendMessage(ChatColor.RED+"EntityType "+subtype[0]+" does not exist");
						    		    		getLogger().log(Level.INFO, "EntityType "+subtype[0]+" does not exist");
						    		    		if (arg0 instanceof Player) 
						    		    			return throwUsage((Player) arg0, arg2);
						    		    		else 
						    		    			return true;
						    				}
			    			    		}
			    			    	}
			    				}
			    			}
		    			}
		    		}
		    	}
		    	else if(types.length > 2)
		    	{
		    		arg0.sendMessage(ChatColor.RED+"Type format invalid");
		    		getLogger().log(Level.INFO, "Type format invalid");
		    		if (arg0 instanceof Player) 
		    			return throwUsage((Player) arg0, arg2);
		    		else 
		    			return true;
		    	}
	    		
		    	for(Entity entity : entities)
		    	{
		    		if(entity.isValid())
		    		{
			    		if(types[0].contains("i"))
			    		{
				    	    if(entity.getType() == org.bukkit.entity.EntityType.DROPPED_ITEM)
				    	    {
				    	    	if(verbose)
	    						{
	    							arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
			    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
	    						}	
				    	    	entity.remove();
				    	    	removed_entities++;
				    	    }
			    		}
			    		if(types[0].contains("m"))
			    		{
			    			if(entity instanceof org.bukkit.entity.Monster)
			    			{
			    				boolean tamed = false;
			    				if(entity instanceof org.bukkit.entity.Tameable)
			    					tamed = ((org.bukkit.entity.Tameable)entity).isTamed();
			    				if(!tamed)
			    				{
				    				if(verbose)
		    						{
				    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
				    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
		    						}	
					    	    	entity.remove();
					    	    	removed_entities++;
			    				}
				    	    }
			    		}
			    		if(types[0].contains("a"))
			    		{
			    			if(entity instanceof org.bukkit.entity.Animals)
			    			{
			    				boolean tamed = false;
			    				if(entity instanceof org.bukkit.entity.Tameable)
			    					tamed = ((org.bukkit.entity.Tameable)entity).isTamed();
			    				if(!tamed)
			    				{
				    				if(verbose)
		    						{
				    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
				    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
		    						}	
					    	    	entity.remove();
					    	    	removed_entities++;
			    				}
				    	    }
			    		}
			    		if(types[0].contains("M"))
			    		{
			    			if(entity instanceof org.bukkit.entity.Monster)
			    			{
			    				boolean tamed = false;
			    				if(entity instanceof org.bukkit.entity.Tameable)
			    					tamed = ((org.bukkit.entity.Tameable)entity).isTamed();
			    				if(tamed)
			    				{
				    				if(verbose)
		    						{
				    					arg0.sendMessage(ChatColor.GRAY+"Removed tamed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
				    		    		getLogger().log(Level.INFO, "Removed tamed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
		    						}	
					    	    	entity.remove();
					    	    	removed_entities++;
			    				}
				    	    }
			    		}
			    		if(types[0].contains("A"))
			    		{
			    			if(entity instanceof org.bukkit.entity.Animals)
			    			{
			    				boolean tamed = false;
			    				if(entity instanceof org.bukkit.entity.Tameable)
			    					tamed = ((org.bukkit.entity.Tameable)entity).isTamed();
			    				if(tamed)
			    				{
				    				if(verbose)
		    						{
				    					arg0.sendMessage(ChatColor.GRAY+"Removed tamed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
				    		    		getLogger().log(Level.INFO, "Removed tamed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
		    						}	
					    	    	entity.remove();
					    	    	removed_entities++;
			    				}
				    	    }
			    		}
			    		if(types[0].contains("n"))
			    		{
			    			if(entity instanceof org.bukkit.entity.NPC)
			    			{
			    				if(verbose)
	    						{
			    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
			    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
	    						}	
				    	    	entity.remove();
				    	    	removed_entities++;
				    	    }
			    		}
			    		if(types[0].contains("v"))
			    		{
			    			if(entity instanceof org.bukkit.entity.Vehicle)
			    			{
			    				if(verbose)
	    						{
			    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
			    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
	    						}	
				    	    	entity.remove();
				    	    	removed_entities++;
				    	    }
			    		}
			    		if(types[0].contains("e"))
			    		{
			    			if(entity instanceof org.bukkit.entity.ExperienceOrb)
			    			{
			    				if(verbose)
	    						{
			    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
			    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
	    						}	
				    	    	entity.remove();
				    	    	removed_entities++;
				    	    }
			    		}
			    		if(types[0].contains("f"))
			    		{
			    			if(entity instanceof org.bukkit.entity.FallingBlock)
			    			{
			    				if(verbose)
	    						{
			    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
			    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
	    						}	
				    	    	entity.remove();
				    	    	removed_entities++;
				    	    }
			    		}
			    		if(entity.isValid()) // x and p are overlapping, this prevents duplicate remove execution
			    		{
				    		if(types[0].contains("x"))
				    		{
				    			if(entity instanceof org.bukkit.entity.Explosive)
				    			{
				    				if(verbose)
		    						{
				    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
				    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
		    						}	
					    	    	entity.remove();
					    	    	removed_entities++;
					    	    }
				    		}
				    		else if(types[0].contains("t")) // x inherits t
				    		{
				    			if(entity instanceof org.bukkit.entity.TNTPrimed)
				    			{
				    				if(verbose)
		    						{
				    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
				    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
		    						}	
					    	    	entity.remove();
					    	    	removed_entities++;
					    	    }
				    		}
			    		}
			    		if(entity.isValid())
			    		{
				    		if(types[0].contains("p"))
				    		{
				    			if(entity instanceof org.bukkit.entity.Projectile)
				    			{
				    				if(verbose)
		    						{
				    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
				    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
		    						}	
					    	    	entity.remove();
					    	    	removed_entities++;
					    	    }
				    		}
				    		else if(types[0].contains("r")) // p inherits r
				    		{
				    			if(entity instanceof org.bukkit.entity.Arrow)
				    			{
				    				if(verbose)
		    						{
				    					arg0.sendMessage(ChatColor.GRAY+"Removed "+entity.getType().name()+" at "+new DecimalFormat("#.###").format(entity.getLocation().getX())+", "+new DecimalFormat("#.###").format(entity.getLocation().getY())+", "+new DecimalFormat("#.###").format(entity.getLocation().getZ()));
				    		    		getLogger().log(Level.INFO, "Removed "+entity.getType().name()+" at "+Double.toString(entity.getLocation().getX())+", "+Double.toString(entity.getLocation().getY())+", "+Double.toString(entity.getLocation().getZ()));
		    						}	
					    	    	entity.remove();
					    	    	removed_entities++;
					    	    }
				    		}
			    		}
		    		}
		    	}
		    	arg0.sendMessage(Integer.toString(removed_entities)+" entities removed");
		    	if (arg0 instanceof Player)
		    	{
		    		org.bukkit.Location slocation = ((Player) arg0).getLocation();
		    		org.bukkit.World sworld = slocation.getWorld();
		    		getLogger().log(Level.INFO, Integer.toString(removed_entities)+" entities in world "+world.getName().toString()+" at "+Double.toString(location.getX())+", "+Double.toString(location.getY())+", "+Double.toString(location.getZ())+" with radius "+radiusx+", "+radiusy+", "+radiusz+" removed by player "+((Player)arg0).getName().toString()+" ("+((Player)arg0).getUniqueId().toString()+") from world "+sworld.getName().toString()+" at "+Double.toString(slocation.getX())+", "+Double.toString(slocation.getY())+", "+Double.toString(slocation.getZ()));
		    	}
    			else if (arg0 instanceof org.bukkit.command.BlockCommandSender)
    			{   				
    				org.bukkit.Location slocation = ((org.bukkit.command.BlockCommandSender) arg0).getBlock().getLocation();
    				org.bukkit.World sworld = slocation.getWorld();
    				getLogger().log(Level.INFO, Integer.toString(removed_entities)+" entities in world "+world.getName().toString()+" at "+Double.toString(location.getX())+", "+Double.toString(location.getY())+", "+Double.toString(location.getZ())+" with radius "+radiusx+", "+radiusy+", "+radiusz+" removed by CommandBlock from world "+sworld.getName().toString()+" at "+Integer.toString(slocation.getBlockX())+", "+Integer.toString(slocation.getBlockY())+", "+Integer.toString(slocation.getBlockZ()));
    			}
    			else
    				getLogger().log(Level.INFO, Integer.toString(removed_entities)+" entities in world "+world.getName().toString()+" at "+Double.toString(location.getX())+", "+Double.toString(location.getY())+", "+Double.toString(location.getZ())+" with radius "+radiusx+", "+radiusy+", "+radiusz+" removed by "+arg0.toString());
    				
	    		return true;
	    	}
	    	if (arg0 instanceof Player)
				return throwUsage((Player) arg0, arg2);
			else
				return true;
	    }
	}
}