# SpigotMC Plugin - Command RemoveEntities #

This Project is published under [GNUGPLv3](https://www.gnu.org/licenses/gpl-3.0.html)

# Command explanation: #

**/removeentities OR**

**/re** <types> <radius or rx:ry:rz> [at location x:y:z] [world]

**<type>** *FLAGS for Entities with optional list of EntityType's*
FLAGS : EntityType , EntityType , EntityType, ...

i - select all dropped items

m - select all monsters except tamed

a - select all animals except tamed

M - select only tamed monsters

A - select only tamed animals

n - select all NPCs

v - select all vehicles

e - select all experience orbs

f - select all falling blocks

x - select all explosives

t - select all primed tnt

p - select all projectiles

r - select all arrows

:[EntityType](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/EntityType.html "spigotmc.org javadocs EntityType"), DROPPED_ITEM.[ItemType](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html "spigotmc.org javadocs Material"), EntityType.[[TAMED|!TAMED]](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Tameable.html "spigotmc.org javadocs Tameable")...

**<radius>** *radius from issuer/commandblock location or the location given in the next paramter*

radius | radiusX : radiusY : radiusZ

**[location]** *optional* - *center location for the radius above*

locationX : locationY : locationZ

**[world]** *optional* - *name of the world to be affected by the command*

worldname

**[verbose]** *optional* - *prints out what and where something was removed*

true|false

** **

**/removeentitiesv and**

**/rev** *same as above including verbose option*

# Permission: #
de.kekse23.removeentities

# Command examples: #
/removeentities i 24

/removeentities :sheep,pig,zombie 24

/removeentities ixp:sheep,pig,zombie 24

/removeentities ixp:sheep,pig,zombie 24 -32:64:-23

/removeentities ixp:sheep,pig,pig_zombie 24 -32:64:-23 world_nether

/removeentities ixp:sheep,pig,pig_zombie 24 -32:64:-23 world_nether true

/removeentitiesv ixp:sheep,dropped_item.diamond,pig,pig_zombie 24 -32:64:-23 world_nether

/removeentitiesv ixpmMA:wolf.!tamed,dropped_item.diamond,horse.tamed,pig_zombie 24 -32:64:-23 world_nether
